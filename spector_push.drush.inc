<?php 

function spector_push_drush_command()
{
  $items = array();
  
  $items['spector-push'] = array(
    'description' => 'Push log entries to spector server',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array()
  );
  
  $items['spector-push-goosegate-download'] = array(
    'description' => 'Download the GooseGate library required when using Sleepy Mongoose',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'options' => array()
  );
  
  return $items;
}

function drush_spector_push()
{
  $settings = variable_get(SPECTOR_PUSH_VAR_SETTINGS, null);
	if (!$settings)
	{
		drush_log('Spector Push is not configured yet. Go to admin/settings/spector_push.', 'error');
		return;
	}
	
	include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'push.php';
	spector_push();
}

function drush_spector_push_goosegate_download()
{
	$libPath = drush_get_context('DRUSH_DRUPAL_ROOT') . DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . 'all' . DIRECTORY_SEPARATOR . 'libraries';
	if (!is_dir($libPath) && !mkdir($libPath))
	{
		drush_log('Could not create libraries folder ' . $libPath, 'error');
		return;
	}
	
	$url = 'http://github.com/theduke/GooseGate/zipball/0.1';
	$localPath = $libPath . DIRECTORY_SEPARATOR . 'goose.zip';
	
	drush_log('Downloading GooseGate from ' . $url . '...', 'info');
	
	if (!copy($url, $localPath))
	{
		drush_log('Could not download library from ' . $url, 'error');
		return;
	}
	
	exec(sprintf('unzip "%s" -d "%s"', $localPath, $libPath));
	
	$dirname = null;
	
	$handle = opendir($libPath);
	while (($file = readdir($handle)) !== false)
	{
		if (strpos($file, 'GooseGate') !== false)
		{
			$dirname = $libPath . DIRECTORY_SEPARATOR . $file;
			break;
		}
	}
	
	if (!$dirname)
	{
		drush_log('Could not find unzipped folder.', 'error');
		return;
	}
	
	rename($dirname, $libPath. DIRECTORY_SEPARATOR . 'GooseGate');
	unlink($localPath);
	
	drush_log('Successfully downloaded GooseGate into sites/all/libraries', 'success');
}
