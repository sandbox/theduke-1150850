<?php

/**
 * Spector Push
 *
 * LICENSE
 *
 * This source file is subject to the GPLv3 license, 
 * available in the LICENSE.txt file bundled with this module or 
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * @package    Spector
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt     GPLv3
 * 
 * @author Christoph Herzog chris@theduke.at
 */


/**
 * Run the push to spector_server
 * 
 * @return boolean true on success
 */
function spector_push()
{
	watchdog('spector_push', 'Starting Push Job.', null, WATCHDOG_INFO);
	
	$settings = variable_get(SPECTOR_PUSH_VAR_SETTINGS, array());
	
	if (!_spector_push_validate_settings($settings))
	{
		watchdog('spector_push', 'Faulty Spector Push settings. Aborting push.', null, WATCHDOG_CRITICAL);
		return;
	}
	
	$lastId = variable_get(SPECTOR_PUSH_VAR_LAST_HANDLED_ID, 0);

	$logEntries = _spector_fetch_logentries($lastId);
	$spectorEntries = _spector_build_entries($logEntries, $settings['project'], $settings['environment']);

	try 
	{ 
		_spector_write_entries($spectorEntries, $settings['method'], $settings);
	} catch (Exception $e) {
		watchdog('spector_push', 'Error while saving log entries: ' . $e->getMessage(), null, WATCHDOG_CRITICAL);
		return false;
	}
	
	watchdog(
	  'spector_push', 
	  'Successfully pushed @count log entries.', 
	  array('@count' => count($logEntries)), 
	  WATCHDOG_INFO);
	  
	return true;
}

/**
 * Get all relevant unpushed log entries from the database.
 * 
 * @param integer $lastId
 * @return array the entries
 */
function _spector_fetch_logentries($lastId)
{
	$q = 'SELECT w.* FROM {watchdog} w WHERE w.wid > %d';
	$result = db_query($q, $lastId);
	
	$entries = array();
	
	while ($entry = db_fetch_object($result))
	{
		$entries[] = $entry;
	}
	
	return $entries;
}

/**
 * Assemble the spector log entry data from dblog entries
 * 
 * @param array $entries
 * @param string $project
 * @param string $env
 * @return array built entrance
 */
function _spector_build_entries($entries, $project, $env)
{
	$spectorEntries = array();
	
	foreach ($entries as $entry)
	{
		$data = array(
			'watchdog_id' => (int) $entry->wid,
		  'link' => $entry->link,
			'location' => $entry->location,
			'referer' => $entry->referer,
			'hostname' => $entry->hostname,
			'userId' => (int) $entry->uid
		);
		$microtime = explode(' ', microtime());
		$microtime = $microtime[0]; 
		
		$spectorEntries[] = array(
			'wid' => (int) $entry->wid,
			'project' => $project,
			'environment' => $env,
			'type' => $entry->type === 'php' ? 'php' : 'log',
			'bucket' => $entry->type,
			'severity' => (int) $entry->severity,
			'message' => _spector_format_message($entry),
			'data' => json_encode($data),
			'time' => (int) $entry->timestamp,
			'microtime' => (float) $microtime
		);
	}
	
	return $spectorEntries;
}

/**
 * Write the assembled log entries to spector server using
 * either Mongo PHP extension or GooseGate
 * 
 * @param array $entries
 * @param string $method see SPECTOR_PUSH_METHOD_* constants
 * @param array $settings
 * @throws Exception
 */
function _spector_write_entries(array $entries, $method, array $settings)
{
	$entryCount = count($entries);
	$batchSize = 20;
	

	if ($method === SPECTOR_PUSH_METHOD_EXTENSION)
	{
		if (!class_exists('Mongo'))
		{
			throw new Exception('MongoDB PHP extension is not installed.');
		}
		
		$mongo = new Mongo($settings['db']);
	} else if ($method === SPECTOR_PUSH_METHOD_MONGOOSE) {
		$docRoot = $_SERVER['DOCUMENT_ROOT'];
    if (!$docRoot && function_exists('drush_get_context'))
    {
      $docRoot = drush_get_context('DRUSH_DRUPAL_ROOT');
    }
    
    $libPath = $docRoot . '/sites/all/libraries/GooseGate/lib/';
    
    if (!is_dir($libPath))
    {
    	throw new Exception('GooseGate library not in sites/all/libraries.');
    }
		
		require_once $libPath . 'GooseGate/HttpClient.php';
		require_once $libPath . 'GooseGate/Mongo.php';
		require_once $libPath . 'GooseGate/MongoCollection.php';
		require_once $libPath . 'GooseGate/MongoCursor.php';
		require_once $libPath . 'GooseGate/MongoDB.php';
		
		$batchSize = 1000;
		
		$mongo = new \GooseGate\Mongo(
			$settings['db'], 
			$settings['mongoose_server'], 
			$settings['username'], 
			$settings['password']);
	}
	
	$collection = $mongo->spector->log_entries;
	
	$start = time();
	
	while (count($entries))
	{
		$startStamp = microtime(true);

		$batch = array_splice($entries, 0, $batchSize);
		$collection->batchInsert($batch);
		$timeTaken = microtime(true) - $startStamp;
		
		watchdog(
		  'spector_push', 
		  'Wrote batch of @count entries in @taken seconds.', 
		  array('@count' => count($batch), '@taken' => $timeTaken),
		  WATCHDOG_DEBUG);
		
		// update last insert variable after each batch to prevent duplicating all
		// entries when a batch fails
		$lastEntryData = json_decode($batch[count($batch) - 1]['data']);
		$lastWid = $lastEntryData->watchdog_id;
		
		//variable_set(SPECTOR_PUSH_VAR_LAST_HANDLED_ID, $lastWid); 
	}
	
	// log some statistics data for debugging
	$timeTaken = time() - $start;
	$avgTime = $timeTaken / (float) $entryCount;
	watchdog(
		'spector_push',
		'Wrote @count entries in @seconds. Average time per entry: @avg',
		array('@count' => $entryCount, '@avg' => $avgTime),
		WATCHDOG_DEBUG
	);
}

/**
 * Validate a settings array
 * 
 * @param array $settings
 * @return boolean true if valid
 */
function _spector_push_validate_settings($settings)
{
	$required = array('enabled', 'db', 'project', 'method', 'environment');
	
	foreach ($required as $key)
	{
		if (!(isset($settings[$key]) && $settings[$key])) return false;
	}
	
	if (!_spector_validate_uri($settings['db'])) return false;
	
	if ($settings['method'] === 'extension')
	{
		
	} else if ($settings['method'] === 'sleepymongoose') {
		if (!_spector_validate_uri($settings['mongoose_server'])) return false;
	} else { return false; }
	
	return true;
}


/**
 * Formats a log message for display.
 *
 * @param $dblog
 *   An object with at least the message and variables properties
 */
function _spector_format_message($dblog) {
  // Legacy messages and user specified text
  if ($dblog->variables === 'N;') {
    $msg = $dblog->message;
  }
  // Message to translate with injected variables
  else {
    $msg = t($dblog->message, unserialize($dblog->variables));
  }
  
  $msg = html_entity_decode($msg);
  return $msg;
}
