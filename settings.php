<?php

/**
 * Spector Push
 *
 * LICENSE
 *
 * This source file is subject to the GPLv3 license 
 * available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * 
 * @package    Spector
 * @license    http://www.gnu.org/licenses/gpl-3.0.txt     GPLv3
 * 
 * @author Christoph Herzog chris@theduke.at
 */

/**
 * Page callback 
 */
function spector_push_config_page()
{
  $output =  drupal_get_form('spector_push_settings_form');
    
  return $output;
}

/**
 * Settings form
 * 
 * @param array $form_state
 * @return array the form 
 */
function spector_push_settings_form(&$form_state)
{
	$settings = variable_get(SPECTOR_PUSH_VAR_SETTINGS, array());

	$form['enabled'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Enable Pushing'),
    '#default_value' => isset($settings['enabled']) ? $settings['enabled'] : ''
  );
  
  $form['project'] = array(
    '#type'  => 'textfield',
    '#required'  => true,
    '#title' => t('Project'),
    '#description' => t('Name for the project that will be used to categorize this sites log messages.'),
    '#default_value' => isset($settings['project']) ? $settings['project'] : ''
  );
  
  $form['environment'] = array(
    '#type'  => 'textfield',
    '#required'  => true,
    '#title' => t('Environment'),
    '#description' => t('Current environment like "development", "production".'),
    '#default_value' => isset($settings['environment']) ? $settings['environment'] : ''
  );
	
  $form['method'] = array(
    '#type'  => 'select',
    '#options' => array(
  		'' => '',
  		'extension' => 'PHP MongoDB Extesnion',
  		'sleepymongoose' => 'Sleepy Mongoose'
  	),
    '#required'  => true,
    '#title' => t('Method'),
  	'#description' => t('If the MongoDB PHP extension is not available on your server, you can use Sleepy Mongoose instead.'),
    '#default_value' => isset($settings['method']) ? $settings['method'] : ''
  );
  
  $form['db'] = array(
    '#type'  => 'textfield',
    '#required'  => true,
    '#title' => t('MongoDB Server'),
    '#description' => t('All mongodb:// URI schemes are supported here, including authentication. For example: mongodb://user:password@localhost:27017'),
    '#default_value' => isset($settings['db']) ? $settings['db'] : ''
  );
  
  $form['mongoose'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Sleepy Mongoose'),
    '#tree'  => false,
    '#collapsible' => true,
  	'#collapsed' => true
  );
  
   $form['mongoose']['mongoose_server'] = array(
    '#type'  => 'textfield',
    '#required'  => false,
    '#title' => t('Sleepy Mongoose Server'),
    '#description' => t('Only applicable if you use Sleepy Mongoose. For example: localhost:27080'),
    '#default_value' => isset($settings['mongoose_server']) ? $settings['mongoose_server'] : ''
  );
  
  $form['mongoose']['username'] = array(
    '#type'  => 'textfield',
    '#title' => t('HTTP Authentication Username'),
    '#default_value' => isset($settings['username']) ? $settings['username'] : ''
  );
  
  $form['mongoose']['password'] = array(
    '#type'  => 'textfield',
    '#title' => t('HTTP Authentication Password'),
    '#default_value' => isset($settings['password']) ? $settings['password'] : ''
  );
  
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit')
  );
  
  return $form;
}

function spector_push_settings_form_validate($form, &$form_state)
{
	$values = $form_state['values'];
	
  if ($values['method'] === SPECTOR_PUSH_METHOD_MONGOOSE)
  {
  	if (!($values['mongoose_server'] && _spector_validate_uri($values['mongoose_server'])))
  	{
  		form_set_error('mongoose_server', t('You have to specify a valid Sleepy Mongoose server.'));
  	}
  	
  	$libPath = $_SERVER['DOCUMENT_ROOT'] . '/sites/all/libraries/GooseGate';
  	if (!is_dir($libPath)) 
  		drupal_set_message(t('GooseGate library is not in sites/all/libraries. Put it there by downloading it manually or running drush spector-push-download-goosegate'), 'error');
  	
  } else if ($values['method'] === SPECTOR_PUSH_METHOD_EXTENSION) {
  	if (!class_exists('Mongo'))
  	{
  		form_set_error('method', t('The MongoDB PHP extension is not installed.'));
  	}
  }
  
  if ($values['username'] || $values['password'])
  {
  	if (!($values['username']) && $values['username'])
  	{
  		form_set_error('authentication', t('You have to specify both username and password.'));
  	}
  }
  
  if (!($values['db'] && _spector_validate_uri($values['db'])))
  {
  	form_set_error('db', t('No or invalid MongoDB Server.'));
  }
}

function spector_push_settings_form_submit($form, &$form_state)
{
  $values = $form_state['values'];
  
	variable_set(SPECTOR_PUSH_VAR_SETTINGS, $values);
	drupal_set_message(t('Settings have been updated.'));	
}