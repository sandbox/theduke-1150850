
-- SUMMARY --

This module integrates Drupal with the Spector log aggregation system (https://github.com/theduke/spector).
It will regularily push the new log entries logged in dblog to your Spector server.

If you have the Mongo PHP extension installed on your Webserver it can be used.
Otherwise you will have to set up Sleepy Mongoose (a REST server for MongoDB) on 
your Spector server and the module will push the log entries via the REST api.

For a full description of the module, visit the project page:
  http://drupal.org/project/spector_push

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/spector_push


-- REQUIREMENTS --

PHP5.3

the dblog module

Either Mongo PHP extension or GooseGate library 
(can be downloaded with bundled drush command, see Installation).
https://github.com/theduke/GooseGate

Notice that if you do want to use Sleepy Mongoose and GooseGate,
for now you will have to use my fork of Sleepy Mongoose until my pull
request is accepted. 
Find my fork here: https://github.com/theduke/sleepy.mongoose

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* If you want to use Sleepy Mongoose, add the GooseGate library (https://github.com/theduke/GooseGate) 
  into sites/all/libraries/GooseGate, 
  or download automatically with drush spector-push-download-goosegate


-- CONFIGURATION --

* Configure push settinsg at admin/settings/spector_push:


-- RUNNING THE PUSH --

Pushes will be done on every cron run, if enabled.
You can manually push by running drush spector-push.

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Christoph Herzog (theduke) - http://drupal.org/user/644976
